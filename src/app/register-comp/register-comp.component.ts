import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule,FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiloginService } from '../apilogin.service';

@Component({
  selector: 'app-register-comp',
  templateUrl: './register-comp.component.html',
  styleUrls: ['./register-comp.component.css']
})
export class RegisterCompComponent implements OnInit {

  isauth: boolean =false;
  RegisterForm: FormGroup;
  email:FormControl;
  password:FormControl;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder,private apiloginService: ApiloginService, private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.RegisterForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      first_name: ['',],
      last_name: ['',],
      address: ['',],
      title:['',],
      dob:['',],
      country:['',],
      city:['',],
      zip:['',]
    });
  }

  register(){

    const valemail = this.RegisterForm.get('email').value;
    const valpassword = this.RegisterForm.get('password').value;
    const valfirstname = this.RegisterForm.get('first_name').value;
    const vallastname = this.RegisterForm.get('last_name').value;
    const valaddress = this.RegisterForm.get('address').value;
    const valtitle = this.RegisterForm.get('title').value;
    const valdob = this.RegisterForm.get('dob').value;
    const valcountry = this.RegisterForm.get('country').value;
    const valcity = this.RegisterForm.get('city').value;
    const valzip = this.RegisterForm.get('zip').value;
    
    const userData = {
      email : valemail,
      password : valpassword,
      first_name : valfirstname,
      last_name : vallastname,
      profile : { 
        address : valaddress,
        title: valtitle,
        dob: valdob,
        country: valcountry,
        city: valcity,
        zip: valzip
      }
    }

    this.apiloginService.registerUser(userData).subscribe(
      (dataFromBack) => {
        console.log(dataFromBack);
        alert('Login Successful');
        this.router.navigate(['login']);
      },
      (error) => {
        this.errorMessage = error;
      }
    );
  }

}

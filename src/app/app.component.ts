import { Component, OnInit } from '@angular/core';
import { ApiloginService } from './apilogin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'djangularlogin';
  isAuth : boolean=false;

  constructor(private apiloginService: ApiloginService) {}
  

  
  

  OnSignOut(){
    this.apiloginService.signOut();
    console.log("la val de local storage"+ localStorage.getItem('token'));
    
  }


}

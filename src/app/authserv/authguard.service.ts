import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardService implements CanActivate {

  constructor(public auth: AuthService, public router: Router) { }

  // canActivate():boolean{
  //   if (!this.auth.isAuthenticated()) {
  //     this.router.navigate(['login']);
  //     return false;
  //   }
  //   return true;
  // }
  //version avec le token en bas
  canActivate( next: ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean{
    if ( localStorage.getItem('token') != null ) 
    return true;
    this.router.navigate(['login']); 
    return false;
  }
}

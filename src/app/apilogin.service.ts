import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})



export class ApiloginService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
		let body = res;
		return body || {};
	}

  registerUser(userData): Observable<any> {
    
	  return this.http.post(endpoint + 'register/',JSON.stringify(userData),httpOptions).pipe(
	    map(this.extractData),
	    catchError(this.handleError<any>('userData'))
	  );
  }
  
  signInUser(userData): Observable<any> {
	  return this.http.post(endpoint + 'login/',JSON.stringify(userData),httpOptions).pipe(
	    map(this.extractData),
	    catchError(this.handleError<any>('userData'))
	  );
  }
  
  private handleError<T> (operation = 'operation', result?: T) {
	  return (error: any): Observable<T> => {

	    // TODO: send the error to remote logging infrastructure
	    console.error(error); // log to console instead

	    // TODO: better job of transforming error for user consumption
	    console.log(`${operation} failed: ${error.message}`);

	    // Let the app keep running by returning an empty result.
	    return of(result as T);
	  };
  	}

  isAuth:boolean=false;

  signOut(){
	  this.isAuth = false;
	  localStorage.removeItem('token');
  }




}
const endpoint = 'http://localhost:8000/api/';
const httpOptions = {
	headers: new HttpHeaders({'Content-Type':  'application/json'})
};
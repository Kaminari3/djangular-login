import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule,FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiloginService } from '../apilogin.service';

@Component({
  selector: 'app-login-comp',
  templateUrl: './login-comp.component.html',
  styleUrls: ['./login-comp.component.css']
})
export class LoginCompComponent implements OnInit {

  // authStatus: boolean;
  LoginForm: FormGroup;
  email:FormControl;
  password:FormControl;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder,private apiloginService: ApiloginService, private router: Router) { }

  ngOnInit() {
    this.initForm();

  }

  initForm() {
    this.LoginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }

  onSubmit() {
    const valemail = this.LoginForm.get('email').value;
    const valpassword = this.LoginForm.get('password').value;
    const userData = {email: valemail, password: valpassword}
    
    this.apiloginService.signInUser(userData).subscribe(
      (dataFromBack) => {
        localStorage.setItem('token',dataFromBack['token']);
        console.log('le token est:'+dataFromBack['token']);
        this.apiloginService.isAuth = true;
        console.log("login value  isauth :"+this.apiloginService.isAuth);
        this.router.navigate(['dashboard']);
      },
      (error) => {
        this.errorMessage = error;
        console.log(error);
      }
    );
  }


}

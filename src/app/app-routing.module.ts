import { NgModule } from '@angular/core';
import { Routes, RouterModule,CanActivate } from '@angular/router';
import { RegisterCompComponent } from './register-comp/register-comp.component';
import { LoginCompComponent } from './login-comp/login-comp.component';
import { DashboardCompComponent } from './dashboard-comp/dashboard-comp.component';
import { from } from 'rxjs';
import { AuthguardService } from './authserv/authguard.service';


const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: 'register', component: RegisterCompComponent },
  { path: 'login', component: LoginCompComponent },
  { path: 'dashboard', canActivate:[AuthguardService],component: DashboardCompComponent  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

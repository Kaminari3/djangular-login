import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginCompComponent } from './login-comp/login-comp.component';
import { RegisterCompComponent } from './register-comp/register-comp.component';
import { MyMaterialModule } from  './material.module';
import { DashboardCompComponent } from './dashboard-comp/dashboard-comp.component';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { ApiloginService } from './apilogin.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './authserv/auth.service';
import { AuthguardService } from './authserv/authguard.service';
import { JwtModule } from "@auth0/angular-jwt";
//import {MatButtonModule,MatToolbarModule} from  '@angular/material';

export function tokenGetter() {
  return localStorage.getItem("token");
}



@NgModule({
  declarations: [
    AppComponent,
    LoginCompComponent,
    RegisterCompComponent,
    DashboardCompComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MyMaterialModule,
    //MatButtonModule,
    //MatToolbarModule
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ["localhost:4200"],
        blacklistedRoutes: ["example.com/examplebadroute/"]
      }
    })
    
  ],
  providers: [ApiloginService,AuthService,AuthguardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
